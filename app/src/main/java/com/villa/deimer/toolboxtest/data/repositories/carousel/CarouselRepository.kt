package com.villa.deimer.toolboxtest.data.repositories.carousel

import com.villa.deimer.toolboxtest.data.dto.CarouselDTO
import com.villa.deimer.toolboxtest.di.carousel.CarouselModule
import com.villa.deimer.toolboxtest.di.carousel.DaggerICarouselComponent
import com.villa.deimer.toolboxtest.network.ApiFactory
import io.reactivex.Observable

class CarouselRepository: ICarouselRepository {

    init {
        DaggerICarouselComponent.builder().carouselModule(CarouselModule()).build().inject(this)
    }

    override fun getCarousels(): Observable<List<CarouselDTO>>? {
        return ApiFactory.build()?.getCarousels()
    }
}
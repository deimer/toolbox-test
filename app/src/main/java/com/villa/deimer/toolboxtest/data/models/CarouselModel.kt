package com.villa.deimer.toolboxtest.data.models

class CarouselModel {
    var title: String? = null
    var type: String? = null
    var items: List<ItemModel>? = null
}
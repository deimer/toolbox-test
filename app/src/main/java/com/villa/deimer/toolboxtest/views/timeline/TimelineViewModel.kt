package com.villa.deimer.toolboxtest.views.timeline

import android.annotation.SuppressLint
import androidx.lifecycle.ViewModel
import com.villa.deimer.toolboxtest.R
import com.villa.deimer.toolboxtest.data.interactors.CarouselInteractor
import com.villa.deimer.toolboxtest.data.models.CarouselModel
import com.villa.deimer.toolboxtest.di.carousel.CarouselModule
import com.villa.deimer.toolboxtest.di.carousel.DaggerICarouselComponent
import com.villa.deimer.toolboxtest.livedata.SingleLiveEvent
import com.villa.deimer.toolboxtest.util.RUtil.Companion.rString
import javax.inject.Inject

@SuppressLint("CheckResult")
class TimelineViewModel: ViewModel() {

    @Inject
    lateinit var carouselInteractor: CarouselInteractor

    init {
        DaggerICarouselComponent.builder().carouselModule(CarouselModule()).build().inject(this)
    }

    var singleLiveEvent: SingleLiveEvent<ViewEvent> = SingleLiveEvent()

    sealed class ViewEvent {
        class ResponseError(val errorMessage: String): ViewEvent()
        class ResponseSuccess(val carousels: List<CarouselModel>): ViewEvent()
    }

    fun getCarousels() {
        carouselInteractor.getCarousels()?.subscribe ({
            if(it.isNotEmpty()) {
                singleLiveEvent.value = ViewEvent.ResponseSuccess(it)
            } else {
                singleLiveEvent.value = ViewEvent.ResponseError(rString(R.string.list_is_empty))
            }
        }, {
            singleLiveEvent.value = ViewEvent.ResponseError(it.message.toString())
        })
    }
}
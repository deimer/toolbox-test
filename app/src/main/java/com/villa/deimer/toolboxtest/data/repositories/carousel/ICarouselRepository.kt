package com.villa.deimer.toolboxtest.data.repositories.carousel

import com.villa.deimer.toolboxtest.data.dto.CarouselDTO
import io.reactivex.Observable

interface ICarouselRepository {

    fun getCarousels(): Observable<List<CarouselDTO>>?
}
package com.villa.deimer.toolboxtest.data.interactors

import com.villa.deimer.toolboxtest.data.dto.CarouselDTO
import com.villa.deimer.toolboxtest.data.dto.ItemDTO
import com.villa.deimer.toolboxtest.data.models.CarouselModel
import com.villa.deimer.toolboxtest.data.models.ItemModel
import com.villa.deimer.toolboxtest.data.repositories.carousel.ICarouselRepository
import com.villa.deimer.toolboxtest.di.carousel.CarouselModule
import com.villa.deimer.toolboxtest.di.carousel.DaggerICarouselComponent
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class CarouselInteractor {

    @Inject
    lateinit var carouselRepository: ICarouselRepository

    init {
        DaggerICarouselComponent.builder().carouselModule(CarouselModule()).build().inject(this)
    }

    fun getCarousels(): Observable<List<CarouselModel>>? {
        return carouselRepository.getCarousels()
            ?.subscribeOn(Schedulers.io())
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.flatMap { dtoList ->
                Observable.just(convertCarouselsDtoListToModels(dtoList))
            }
    }

    private fun convertCarouselsDtoListToModels(dtoList: List<CarouselDTO>): List<CarouselModel> {
        val models = mutableListOf<CarouselModel>()
        dtoList.forEach { dto ->
            val model = CarouselModel().apply {
                title = dto.title
                type = dto.type
                items = convertItemsDtoToModels(dto.items)
            }
            models.add(model)
        }
        return models
    }

    private fun convertItemsDtoToModels(dtoList: List<ItemDTO>?): List<ItemModel> {
        val models = mutableListOf<ItemModel>()
        dtoList?.forEach { dto ->
            val model = ItemModel().apply {
                title = dto.title
                url = dto.url
                video = dto.video
            }
            models.add(model)
        }
        return models
    }
}
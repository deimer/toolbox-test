package com.villa.deimer.toolboxtest.data.dto

import com.google.gson.annotations.SerializedName

class ItemDTO {

    @SerializedName("title")
    var title: String? = null
    @SerializedName("url")
    var url: String? = null
    @SerializedName("video")
    var video: String? = null
}
package com.villa.deimer.toolboxtest.views.welcome

import android.animation.Animator
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.daimajia.androidanimations.library.Techniques
import com.daimajia.androidanimations.library.YoYo
import com.villa.deimer.toolboxtest.R
import com.villa.deimer.toolboxtest.views.timeline.TimelineActivity
import kotlinx.android.synthetic.main.activity_welcome.*

class WelcomeActivity : FragmentActivity() {

    private lateinit var welcomeViewModel: WelcomeViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_welcome)
        setupView()
    }

    private fun setupView() {
        setupViewModel()
        initSubscriptions()
    }

    private fun setupViewModel() {
        welcomeViewModel = ViewModelProviders.of(this).get(WelcomeViewModel::class.java)
        welcomeViewModel.launchAnimationLogo(700)
    }

    private fun initSubscriptions() {
        welcomeViewModel.singleLiveEvent.observe(this, Observer {
            when(it) {
                is WelcomeViewModel.ViewEvent.AnimationLogo -> {
                    animateLogo()
                }
            }
        })
    }

    private fun animateLogo() {
        YoYo.with(Techniques.SlideInUp)
            .duration(700)
            .withListener(object : Animator.AnimatorListener {
                override fun onAnimationStart(animation: Animator) {}
                override fun onAnimationCancel(animation: Animator) {}
                override fun onAnimationRepeat(animation: Animator) {}
                override fun onAnimationEnd(animation: Animator) {
                    animateLogoPulse()
                }
            }).playOn(imgLogo)
        imgLogo.visibility = View.VISIBLE
    }

    private fun animateLogoPulse() {
        YoYo.with(Techniques.Tada)
            .duration(700)
            .withListener(object : Animator.AnimatorListener {
                override fun onAnimationStart(animation: Animator) {}
                override fun onAnimationCancel(animation: Animator) {}
                override fun onAnimationRepeat(animation: Animator) {}
                override fun onAnimationEnd(animation: Animator) {
                    openActivityTimeline()
                }
            })
            .playOn(imgLogo)
    }

    private fun openActivityTimeline() {
        startActivity(Intent(this, TimelineActivity::class.java))
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
        finish()
    }
}

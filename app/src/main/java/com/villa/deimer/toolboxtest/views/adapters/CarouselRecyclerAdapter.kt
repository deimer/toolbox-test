package com.villa.deimer.toolboxtest.views.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.villa.deimer.toolboxtest.R
import com.villa.deimer.toolboxtest.data.models.CarouselModel
import com.villa.deimer.toolboxtest.data.models.ItemModel
import com.villa.deimer.toolboxtest.util.RUtil.Companion.rString
import kotlinx.android.synthetic.main.item_carousel_view.view.*

class CarouselRecyclerAdapter(
    private val context: Context,
    private val carousels: List<CarouselModel>
): RecyclerView.Adapter<ViewHolderCarousel>() {

    override fun getItemCount(): Int {
        return carousels.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolderCarousel {
        return ViewHolderCarousel(LayoutInflater.from(context)
            .inflate(R.layout.item_carousel_view, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolderCarousel, position: Int) {
        val carousel = carousels[position]
        holder.lblName.text = "${carousel.title}"
        carousel.items?.let { setupRecyclerItems(holder.recyclerItems, it, carousel.type) }
    }

    private fun setupRecyclerItems(recyclerItems: RecyclerView, items: List<ItemModel>, type: String?) {
        val linearLayoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        recyclerItems.layoutManager = linearLayoutManager
        if(type == rString(R.string.lbl_poster)) {
            recyclerItems.adapter = PosterRecyclerAdapter(context, items)
        } else {
            recyclerItems.adapter = ThumbRecyclerAdapter(context, items)
        }
    }
}

class ViewHolderCarousel (view: View): RecyclerView.ViewHolder(view) {
    val lblName: TextView = view.lblCarouselName
    val recyclerItems: RecyclerView = view.recyclerItems
}
package com.villa.deimer.toolboxtest.util

import com.villa.deimer.toolboxtest.ToolboxApplication

class RUtil {

    companion object {
        fun rString(resId: Int): String {
            return ToolboxApplication.getInstance().getString(resId)
        }
    }
}
package com.villa.deimer.toolboxtest

import android.app.Application
import android.content.Context

class ToolboxApplication: Application() {

    override fun onCreate() {
        super.onCreate()
        instance = this
    }

    companion object {
        private lateinit var instance: ToolboxApplication

        fun getInstance(): ToolboxApplication {
            return instance
        }

        fun getAppContext(): Context {
            return instance.applicationContext
        }
    }
}
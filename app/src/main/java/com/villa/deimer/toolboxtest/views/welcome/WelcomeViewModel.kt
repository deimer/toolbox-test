package com.villa.deimer.toolboxtest.views.welcome

import android.os.Handler
import androidx.lifecycle.ViewModel
import com.villa.deimer.toolboxtest.livedata.SingleLiveEvent

class WelcomeViewModel: ViewModel() {

    var singleLiveEvent: SingleLiveEvent<ViewEvent> = SingleLiveEvent()

    sealed class ViewEvent {
        class AnimationLogo (val success: Boolean): ViewEvent()
        class ActivityTransaction(val success: Boolean): ViewEvent()
    }

    fun launchAnimationLogo(timeMs: Long) {
        Handler().postDelayed({
            singleLiveEvent.value = ViewEvent.AnimationLogo(true)
        }, timeMs)
    }

    fun launchActivityTransaction(timeMs: Long) {
        Handler().postDelayed({
            singleLiveEvent.value = ViewEvent.ActivityTransaction(true)
        }, timeMs)
    }
}
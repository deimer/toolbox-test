package com.villa.deimer.toolboxtest.data.dto

import com.google.gson.annotations.SerializedName

class CarouselDTO {

    @SerializedName("title")
    var title: String? = null
    @SerializedName("type")
    var type: String? = null
    @SerializedName("items")
    var items: List<ItemDTO>? = null
}
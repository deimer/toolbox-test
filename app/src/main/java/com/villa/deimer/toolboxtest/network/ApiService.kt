package com.villa.deimer.toolboxtest.network

import com.villa.deimer.toolboxtest.data.dto.CarouselDTO
import io.reactivex.Observable
import retrofit2.http.GET

interface ApiService {

    @GET("v2/5d6e1a033200005500a8a5b6")
    fun getCarousels(): Observable<List<CarouselDTO>>
}
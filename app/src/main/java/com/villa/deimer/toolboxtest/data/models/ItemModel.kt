package com.villa.deimer.toolboxtest.data.models

class ItemModel {
    var title: String? = null
    var url: String? = null
    var video: String? = null

    override fun toString(): String {
        return "ItemModel(title=$title, url=$url, video=$video)"
    }
}
package com.villa.deimer.toolboxtest.views.timeline

import android.app.AlertDialog
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.Snackbar
import com.villa.deimer.toolboxtest.R
import com.villa.deimer.toolboxtest.data.models.CarouselModel
import com.villa.deimer.toolboxtest.views.adapters.CarouselRecyclerAdapter
import dmax.dialog.SpotsDialog
import kotlinx.android.synthetic.main.content_timeline.*

class TimelineActivity : AppCompatActivity() {

    private lateinit var timelineViewModel: TimelineViewModel

    private lateinit var dialog: AlertDialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_timeline)
        setupView()
    }

    private fun setupView() {
        title = getString(R.string.title_timeline)
        setupDialogProgress()
        setupViewModel()
        initSubscriptions()
    }

    private fun setupDialogProgress() {
        dialog = SpotsDialog.Builder()
            .setContext(this)
            .setMessage(R.string.get_data_message_progress)
            .setCancelable(false)
            .build()
            .apply { show() }
    }

    private fun setupViewModel() {
        timelineViewModel = ViewModelProviders.of(this).get(TimelineViewModel::class.java)
        timelineViewModel.getCarousels()
    }

    private fun initSubscriptions() {
        timelineViewModel.singleLiveEvent.observe(this, Observer {
            when(it) {
                is TimelineViewModel.ViewEvent.ResponseSuccess -> {
                    dialog.dismiss()
                    setupRecyclerCarousel(it.carousels)
                }
                is TimelineViewModel.ViewEvent.ResponseError -> {
                    dialog.dismiss()
                    Snackbar.make(recyclerCarousel, it.errorMessage, Snackbar.LENGTH_LONG).show()
                }
            }
        })
    }

    private fun setupRecyclerCarousel(carousel: List<CarouselModel>) {
        val adapter = CarouselRecyclerAdapter(this, carousel)
        val linearLayoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        recyclerCarousel.layoutManager = linearLayoutManager
        recyclerCarousel.adapter = adapter
    }
}

package com.villa.deimer.toolboxtest.di.carousel

import com.villa.deimer.toolboxtest.data.interactors.CarouselInteractor
import com.villa.deimer.toolboxtest.data.repositories.carousel.CarouselRepository
import com.villa.deimer.toolboxtest.views.timeline.TimelineViewModel
import dagger.Component

@Component(modules = [CarouselModule::class])
interface ICarouselComponent {

    fun inject(carouselRepository: CarouselRepository)
    fun inject(carouselInteractor: CarouselInteractor)
    fun inject(timelineViewModel: TimelineViewModel)
}
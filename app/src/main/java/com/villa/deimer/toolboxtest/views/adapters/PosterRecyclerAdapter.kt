package com.villa.deimer.toolboxtest.views.adapters

import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import com.villa.deimer.toolboxtest.R
import com.villa.deimer.toolboxtest.data.models.ItemModel
import com.villa.deimer.toolboxtest.util.RUtil.Companion.rString
import com.villa.deimer.toolboxtest.views.video.VideoActivity
import kotlinx.android.synthetic.main.item_carousel_card_poster.view.*
import kotlinx.android.synthetic.main.item_carousel_card_poster.view.imgCover
import kotlinx.android.synthetic.main.item_carousel_card_poster.view.lblName

class PosterRecyclerAdapter(
    private val context: Context,
    private val items: List<ItemModel>
): RecyclerView.Adapter<ViewHolderPoster>() {

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolderPoster {
        return ViewHolderPoster(LayoutInflater.from(context)
                .inflate(R.layout.item_carousel_card_poster, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolderPoster, position: Int) {
        val item = items[position]
        holder.lblName.text = "${item.title}"
        openDetail(holder.cardItem, item.video)
        setImage(holder.imgCover, item.url)
    }

    private fun openDetail(cardItem: CardView, urlVideo: String?) {
        cardItem.setOnClickListener {
            if(urlVideo.isNullOrEmpty()) {
                showDialog()
            } else {
                openVideo(urlVideo)
            }
        }
    }

    private fun showDialog() {
        AlertDialog.Builder(context).apply {
            setTitle(rString(R.string.error_video))
            setCancelable(true)
            setPositiveButton(rString(R.string.ok)) { dialog, _ ->
                dialog.dismiss()
            }
            show()
        }
    }

    private fun openVideo(urlVideo: String?) {
        val intent = Intent(context, VideoActivity::class.java)
        intent.putExtra(rString(R.string.extra_video), urlVideo)
        context.startActivity(intent)
    }

    private fun setImage(imgCover: ImageView, url: String?) {
        Picasso.get()
            .load(url)
            .fit()
            .centerCrop()
            .placeholder(R.mipmap.ic_launcher_round)
            .error(R.mipmap.ic_launcher_round)
            .into(imgCover)
    }
}

class ViewHolderPoster(view: View): RecyclerView.ViewHolder(view) {
    val cardItem: CardView = view.cardItemPoster
    val lblName: TextView = view.lblName
    val imgCover: ImageView = view.imgCover
}
package com.villa.deimer.toolboxtest.di.carousel

import com.villa.deimer.toolboxtest.data.interactors.CarouselInteractor
import com.villa.deimer.toolboxtest.data.repositories.carousel.CarouselRepository
import com.villa.deimer.toolboxtest.data.repositories.carousel.ICarouselRepository
import dagger.Module
import dagger.Provides

@Module
class CarouselModule {

    @Provides
    fun provideCarouselRepository(): ICarouselRepository {
        return CarouselRepository()
    }
    @Provides
    fun provideCarouselInteractor(): CarouselInteractor {
        return CarouselInteractor()
    }
}
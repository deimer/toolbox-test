package com.villa.deimer.toolboxtest.views.video

import android.net.Uri
import android.os.Bundle
import android.view.WindowManager
import android.widget.MediaController
import androidx.fragment.app.FragmentActivity
import com.villa.deimer.toolboxtest.R
import com.villa.deimer.toolboxtest.util.RUtil.Companion.rString
import kotlinx.android.synthetic.main.activity_video.*

class VideoActivity : FragmentActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_video)
        setupView()
    }

    private fun setupView() {
        setFullScreen()
        setupVideo()
    }

    private fun setFullScreen() {
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN)
    }

    private fun setupVideo() {
        val urlVideo = intent.getStringExtra(rString(R.string.extra_video))
        val uriVideo = Uri.parse(urlVideo)
        videoView.setVideoURI(uriVideo)
        val mediaController = MediaController(this)
        mediaController.setAnchorView(videoView)
        videoView.start()
    }
}
